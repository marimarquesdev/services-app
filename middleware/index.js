var Service = require("../models/services");
var Comment = require("../models/comment");

var middlewareObj = {};

middlewareObj.checkServiceOwnership = function(req, res, next){
    if(req.isAuthenticated()){
        Service.findById(req.params.id, function(err, foundService){
            if(err){
                res.redirect("back");
            }else{
                if(foundService.author.id.equals(res.user._id)){
                    next();
                }else{
                    res.redirect("back");
                }
            }
        });
    }else{
        res.redirect("back");
    }
}

middlewareObj.checkCommentOwnership = function(req, res, next){
    if(req.isAuthenticated()){
        Comment.findById(req.params.comment.id, function(err, foundComment){
            if(err){
                res.redirect("back");
            }else{
                if(foundComment.author.id.equals(res.user._id)){
                    next();
                }else{
                    res.redirect("back");
                }
            }
        });
    }else{
        res.redirect("back");
    }
}

middlewareObj.isLoggedIn = function(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    req.flash("error", "Please Login First!");
    res.redirect("/login");
}

module.exports = middlewareObj;