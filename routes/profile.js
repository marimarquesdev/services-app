var express = require('express');
var router = express.Router();
var Service = require("../models/services");
var User = require("../models/user");
var middleware = require("../middleware");
var mongoose = require("mongoose");

router.get('/', middleware.isLoggedIn, function(req, res) {
    // Get user info from DB
            Service.find({},function(err, services){
                if(err){
                    console.log(err);
                }else{
                    console.log(services);
                    res.render('profile/index', {services: services});
                }
            });
  });

  module.exports = router;