var express = require('express');
var router = express.Router();
var passport = require("passport");
var User = require("../models/user");

// GET home page
router.get('/', function(req, res) {
  res.render('landing');
});

//Show register form
router.get("/register",function(req, res){
  res.render("register");
});

//Handle sign up logic
router.post("/register",function(req, res){
  var newUser = new User({
    username: req.body.email,
    usertype: req.body.type,
  }); 
  User.register(newUser, req.body.password, function(err, user){
    if(err){
      req.flash("error", err.message);
      return res.render("register");
    }
    passport.authenticate("local")(req, res, function(){
      req.flash("success", "Welcome to Services " + user.username);
      res.redirect("/services");
    });
  });
});

//Show login form
router.get("/login", function(req, res){
  res.render("login", {message: req.flash("error")});
});

// Handling login logic
router.post("/login",passport.authenticate("local", 
{
  successRedirect: "/services",
  failureRedirect: "/login"}), 
function(req, res){
});

// Logout route
router.get("/logout", function(req, res){
  req.logout();
  res.redirect("/services");
});

//middleware
function isLoggedIn(req, res, next){
  if(req.isAuthenticated()){
    return next();
  }
  res.redirect("/login");
}

module.exports = router;
