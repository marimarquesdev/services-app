var express = require('express');
var router = express.Router();
var Service = require("../models/services");
var middleware = require("../middleware");

//INDEX - show all services
router.get('/', function(req, res) {
    // Get all services form DB
    Service.find({},function(err, allServices){
        if(err){
            console.log(err);
        }else{
            res.render('services/index', {services: allServices});
        }
    });
  });

  //INDEX - search services
router.post('/search', function(req, res) {
var query = {name: req.body.name};
  // Get all services form DB
  if(query.name !== ''){
    Service.find(query, function(err, foundServices){
      if(err){
          console.log(err);
      }else{
          res.render('services/index', {services: foundServices});
      }
  });
  }else{
    Service.find({},function(err, foundServices){
      if(err){
        console.log(err);
      }else{
          res.render('services/index', {services: foundServices});
      }
    });
  }
});

  //CREATE - add new service DB
  router.post("/", middleware.isLoggedIn, function(req, res){
    //get date from form and add to services
    var name = req.body.name;
    var type = req.body.type;
    var image = req.body.image;
    var description = req.body.description;
    var author = {
        id: req.user.id,
        username: req.user.username
    }
    var newService = {name:name, type:type, image:image, description:description, author:author}
    //Create a new service and save to the database
    Service.create(newService, function(err, service){
      if(err){
        console.log(err);
      }else{
        //redirect to services page 
        console.log(service);
        res.redirect("/services");
      }
    });
  });

  //NEW - show form to create new service
  router.get("/new",middleware.isLoggedIn, function(req, res){
    res.render("services/new");
  });

  // SHOW - shows more info about one service
  router.get("/:id", function(req, res){
    Service.findById(req.params.id).populate("comments").exec(function(err, foundService){
      if(err){
        console.log(err);
      }else{
        console.log(foundService);
        res.render("services/show", {service:foundService});
      }
    });
  });

  // EDIT SERVICE ROUTE
router.get("/:id/edit", function(req, res){
    Service.findById(req.params.id, function(err, foundService){
      if(err){
        res.redirect("/services");
      }else{
        res.render("services/edit", {service: foundService});
      }
    });
});

// UPDATE SERVICE ROUTE
router.put("/:id", function(req, res){
    // find and update the correct service
    Service.findByIdAndUpdate(req.params.id, req.body.service, function(err, updatedService){
       if(err){
           res.redirect("/services");
       } else {
           //redirect somewhere(show page)
           res.redirect("/services/" + req.params.id);
       }
    });
});

// DESTROY SERVICE ROUTE
router.delete("/:id", function(req, res){
   Service.findByIdAndRemove(req.params.id, function(err){
      if(err){
          res.redirect("/services");
      } else {
          res.redirect("/services");
      }
   });
});

module.exports = router;


