var mongoose = require("mongoose");
var Service = require("./models/services");
var Comment = require("./models/comment");

var data =[
    {
        name: "Mary Brown",
        type: "Translator",
        image: "images/translator.jpg",
        description: "Translation from English/Portuguese and French/Portuguese"
    },
    {
        name: "Jonh Smith",
        type: "Grafic Design",
        image: "images/design.jpg",
        description: "Expert in product design!"
    },
    {
        name: "Luiz Silva",
        type: "WordPress Developer",
        image: "images/wordpress.jpg",
        description: "Develope for you an amazing website!"
    }
]

function seedDB(){
    //remove all services
    Service.remove({}, function(err){
        if(err){
            console.log(err);
        }
        console.log("removed campgrounds!");
        //add a few services
        data.forEach(function(seed){
            Service.create(seed, function(err, service){
                if(err){
                    console.log(err);
                }else{
                    console.log("added a service!");
                    Comment.create({
                        text: "Fast and well executed!",
                        author: "Homer"
                }, function(err, comment){
                    if(err){
                        console.log(err);
                    }else{
                        service.comments.push(comment);
                        service.save();
                        console.log("Created new comment");
                    }
                });
                }
            });
        });
    });
}

module.exports = seedDB;